Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: SNI Proxy
Source: https://github.com/dlundquist/sniproxy/

Files: *
Copyright: 2010-2018 Dustin Lundquist <dustin@null-ptr.net>
           2011 Manuel Kasper <mk@neon1.net>
           2013 Andrej Manduch <AManduch@gmail.com>
           2013 Arni Birgisson <arnib@arnib.net>
           2013 Bram Gotink <bram.gotink@vtk.be>
           2013 Christopher Galtenberg <cgaltenberg@bluebox.net>
           2013 Nick Kugaevsky <nick@kugaevsky.ru>
           2014 Andrej Manduch <amanduch@gmail.com>
           2014 Bearnard Hibbins <bearnard@gmail.com>
           2014 Chris Lundquist <chris.lundquist@github.com>
           2014 Long Hao <imlonghao@gmail.com>
           2014 Lars Reemts <lars.reemts@finalbit.de>
           2014 Naveen Nathan <naveen@lastninja.net>
           2014 Nikos Mavrogiannopoulos <nmav@gnutls.org>
           2014 RickieL <rickie622@gmail.com>
           2014 Thomas Nordquist <thomas.nordquist@viison.com>
           2015 Robin Balyan <balyanrobin@gmail.com>
           2015 Théophile Helleboid <chtitux@gmail.com>
           2016 Aaron Schrab <aaron@schrab.com>
           2016 Peter van Dijk <peter.van.dijk@powerdns.com>
           2016 Remi Gacogne <remi.gacogne@powerdns.com>
           2016 Udit Raikwar <udit043@users.noreply.github.com>
           2016 Zhang Sen <senorsen.zhang@gmail.com>
           2017 Pieter Lexis <pieter@plexis.eu>
           2017 Sebastian Wiedenroth <wiedi@frubar.net>
           2017 Новгородов Игорь <i.novgorodov@maximatelecom.ru>
           2018 Dustin Lundquist <dustin@null-ptr.net>
           2018 Kirill Ponomarev <krion@freebsd.org>
           2018 Oldřich Jedlička <oldium.pro@gmail.com>
           2018 Vit Herman <vit@herman.pro>
License: BSD-2

Files: debian/*
Copyright: 2013 Andreas Loibl <andreas@andreas-loibl.de>
           2013-2017 Dustin Lundquist <dustin@null-ptr.net>
           2014 Long Hao <imlonghao@gmail.com>
           2017-2020 Jan Dittberner <jandd@debian.org>
License: BSD-2

Files: debian/patches/01_fix_gcc-10-build.patch
Copyright: 2020 Pierre-Olivier Mercier <nemunaire@nemunai.re>
License: BSD-2

License: BSD-2
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE HOLDERS OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
